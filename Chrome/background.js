chrome.runtime.onInstalled.addListener(function(){
    var email = "null@viacesi.fr"
    chrome.storage.sync.set({email: email}, function() {
        console.log(`Email is set to ${email}`);
    })

    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: {hostEquals: 'wayf.cesi.fr'}
            })],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});