function saveOptions(e){
        e.preventDefault();
        browser.storage.sync.set({
                email: document.querySelector("#email").value
        });
}

function restoreOptions(){

        function setCurrentChoice(result) {
                document.querySelector("#email").value = result.email || "null@viacesi.fr";
        }

        function onError(error) {
                console.log(`Error: ${error}`);
        }

        var getting = browser.storage.sync.get("email");
        getting.then(setCurrentChoice, onError);

}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);